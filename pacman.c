#include <SDL.h>

#include <stdio.h>
#include <stdbool.h>

SDL_Window *pWindow = NULL;
SDL_Surface *win_surf = NULL;
SDL_Surface *plancheSprites = NULL;

SDL_Rect src_bg = {200, 3, 168, 216}; // x,y, w,h (0,0) en haut a gauche
SDL_Rect bg = {4, 4, 672, 864};       // ici scale x4

// ghost
SDL_Rect ghost_r = {3, 123, 16, 16};
SDL_Rect ghost_l = {37, 123, 16, 16};
SDL_Rect ghost_d = {105, 123, 16, 16};
SDL_Rect ghost_u = {71, 123, 16, 16};
SDL_Rect ghost = {34, 34, 32, 32}; // ici scale x2

// pacman
SDL_Rect pacman;
SDL_Rect sprites_pacman[4][3] = {
    {{2.5, 89, 16, 16}, {46.5, 89, 16, 16}, {62.5, 89, 12, 16}},    // Gauche
    {{2.5, 89, 16, 16}, {19.5, 89, 16, 16}, {34.5, 89, 12, 16}},    // Droite
    {{2.5, 89, 16, 16}, {75, 89.5, 16, 16}, {92, 93, 16, 12}},      // Haut
    {{2.5, 89, 16, 16}, {109, 89.5, 16, 16}, {126, 93.5, 16, 12}}}; // Bas

int count;
int pacman_count;
int num_sprite = 0;
int direction = -1;   // 0: gauche, 1: droite, 2: haut, 3: bas
bool animate = false; // Variable pour gérer l'animation des sprites de Pacman
int x = 322;          // Coordonnée x de Pacman
int y = 640;          // Coordonnée y de Pacman

#include <stdio.h>

void init()
{
    pWindow = SDL_CreateWindow("PacMan", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 700, 900, SDL_WINDOW_SHOWN);
    win_surf = SDL_GetWindowSurface(pWindow);

    plancheSprites = SDL_LoadBMP("./pacman_sprites.bmp");
    count = 0;

    char input[] = "xxxxxxxxxxxxxxxxxxxxx\n\
    x.........x.........x\n\
    x.xxx.xxx.x.xxx.xxx.x\n\
    xox x.x x.x.x x.x xox\n\
    x.xxx.xxx.x.xxx.xxx.x\n\
    x...................x\n\
    x.xxx.x.xxxxx.x.xxx.x\n\
    x.xxx.x.xxxxx.x.xxx.x\n\
    x.....x...x...x.....x\n\
    xxxxx.xxx x xxx.xxxxx\n\
        x.x   0   x.x    \n\
        x.x xxdxx x.x    \n\
    xxxxx.x x 4 x x.xxxxx\n\
    l    .  x123x  .    r\n\
    xxxxx.x xxxxx x.xxxxx\n\
        x.x       x.x    \n\
        x.x xxxxx x.x    \n\
    xxxxx.x xxxxx x.xxxxx\n\
    x.........x.........x\n\
    x.xxx.xxx.x.xxx.xxx.x\n\
    xo..x.....p.....x..ox\n\
    xxx.x.x.xxxxx.x.x.xxx\n\
    xxx.x.x.xxxxx.x.x.xxx\n\
    x.....x...x...x.....x\n\
    x.xxxxxxx.x.xxxxxxx.x\n\
    x...................x\n\
    xxxxxxxxxxxxxxxxxxxxx";

    // Calculer les dimensions du tableau
    int rows = 21;
    int columns = 27;

    // Initialiser le tableau à deux dimensions
    char tableau[rows][columns];

    // Remplir le tableau à partir de la chaîne de caractères
    int index = 0;
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < columns; j++)
        {
            tableau[i][j] = input[index];
            index++;
        }
        // Ignorer le caractère de nouvelle ligne ('\n')
        index++;
    }
}

void updatePacman(int *x, int *y)
{
    pacman = sprites_pacman[direction][num_sprite];
    switch (direction)
    {
    case 0: // Gauche
        *x -= 5;
        break;
    case 1: // Droite
        *x += 5;
        break;
    case 2: // Haut
        *y -= 5;
        break;
    case 3: // Bas
        *y += 5;
        break;
    default:
        pacman = sprites_pacman[0][0];
        break;
    }
}

// fonction qui met à jour la surface de la fenetre "win_surf"
void draw()
{
    SDL_SetColorKey(plancheSprites, false, 0);
    SDL_BlitScaled(plancheSprites, &src_bg, win_surf, &bg);

    SDL_Rect *ghost_in = NULL;
    switch (count / 128)
    {
    case 0:
        ghost_in = &(ghost_r);
        ghost.x++;
        break;
    case 1:
        ghost_in = &(ghost_d);
        ghost.y++;
        break;
    case 2:
        ghost_in = &(ghost_l);
        ghost.x--;
        break;
    case 3:
        ghost_in = &(ghost_u);
        ghost.y--;
        break;
    }
    count = (count + 1) % (512);

    SDL_Rect ghost_in2 = *ghost_in;
    if ((count / 4) % 2)
        ghost_in2.x += 17;

    SDL_SetColorKey(plancheSprites, true, 0);
    SDL_BlitScaled(plancheSprites, &ghost_in2, win_surf, &ghost);

    SDL_Rect pacman_dest = {x, y, 32, 32};
    SDL_BlitScaled(plancheSprites, &pacman, win_surf, &pacman_dest);
}

int main(int argc, char **argv)
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        fprintf(stderr, "Echec de l'initialisation de la SDL %s", SDL_GetError());
        return 1;
    }

    init();

    bool quit = false;
    while (!quit)
    {
        SDL_Event event;
        while (!quit && SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    int mouse_x = event.button.x;
                    int mouse_y = event.button.y;
                    printf("Coordonnées x: %d, y: %d\n", mouse_x, mouse_y);
                }
                break;
            default:
                break;
            }
        }
        int nbk;
        const Uint8 *keys = SDL_GetKeyboardState(&nbk);
        if (keys[SDL_SCANCODE_LEFT])
        {
            direction = 0;
        }
        else if (keys[SDL_SCANCODE_RIGHT])
        {
            direction = 1;
        }
        else if (keys[SDL_SCANCODE_UP])
        {
            direction = 2;
        }
        else if (keys[SDL_SCANCODE_DOWN])
        {
            direction = 3;
        }
        if (keys[SDL_SCANCODE_ESCAPE])
        {
            quit = true;
        }

        if (pacman_count == 2)
        {
            pacman_count = 0;
            num_sprite = num_sprite == 2 ? 0 : num_sprite + 1;
            updatePacman(&x, &y);
        }
        else
        {
            pacman_count++;
        }

        draw();

        SDL_Delay(20); // ~50 fps use SDL_GetTicks64() pour plus de precision
        SDL_UpdateWindowSurface(pWindow);
    }
    SDL_Quit();
    return 0;
}